package io.planner.chip0llino.eventplanner

import io.planner.chip0llino.eventplanner.data.DbTypeConverters
import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun conversionFromIntArray() {
        var arr: Array<Int> = intArrayOf(1,2).toTypedArray()
        val test1 = DbTypeConverters().intArrayToString(arrayOf())
        val test2 = DbTypeConverters().fromStringToArray(test1)
        assertEquals(1, test2[0])
    }

    @Test
    fun conversionToIntArray() {
        var arr: Array<Int> = intArrayOf(1,2).toTypedArray()
        assertEquals(intArrayOf(1,2).toTypedArray(), DbTypeConverters().fromStringToArray("1!2"))
    }
}
