package io.planner.chip0llino.eventplanner.data

import android.arch.persistence.room.TypeConverter
import java.util.*
import kotlin.collections.HashMap


class DbTypeConverters {
    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return if (value == null) null else Date(value)
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return (date?.time)!!.toLong()
    }

     @TypeConverter
     fun fromStringToArray(value: String): Array<Long>
     {
         return if (value.isNotEmpty()) {
             value.split("!").map { it.toLong() }.toTypedArray()
         } else arrayOf()
     }

     @TypeConverter
     fun intArrayToString(value: Array<Long>): String
     {
         return value.toLongArray().joinToString("!")
     }

     @TypeConverter
     fun hashMapToString(value: HashMap<Long, String>): String
     {
         var result = ""

         for (item in value)
         {
             result = "$result^${item.key}&${item.value}"
         }

         if(result[0] == '^')
         {
             result = result.substring(1)
         }
         return result
     }

    @TypeConverter
    fun stringToHashMap(value: String): HashMap<Long, String>
    {
        val map: HashMap<Long, String> = HashMap()
        val lines = value.split("^")
        for(line in lines)
        {
            val intValue = line.substringBefore('&').toLong()
            val stringValue = line.substringAfter('&')
            map[intValue] = stringValue
        }
        return map
    }

    @TypeConverter
    fun hashMapLongToString(value: HashMap<Long, Long>): String
    {
        var result = ""

        for (item in value)
        {
            result = "$result^${item.key}&${item.value}"
        }

        if(result.length > 1) {
            if (result[0] == '^') {
                result = result.substring(1)
            }
        }
        return result
    }

    @TypeConverter
    fun stringToHashMapLong(value: String): HashMap<Long, Long>
    {
        val map: HashMap<Long, Long> = HashMap()
        val lines = value.split("^")

        if(lines.isNotEmpty())
        {
            for(line in lines)
            {
                try {
                    val key = line.substringBefore('&').toLong()
                    val mValue = line.substringAfter('&').toLong()
                    map[key] = mValue
                }
                catch (e: Exception)
                {
                    return map
                }
            }
        }
        return map
    }
}