package io.planner.chip0llino.eventplanner.data.dao

import android.arch.persistence.room.*
import io.planner.chip0llino.eventplanner.data.entities.CalendarEvent

@Dao
interface CalendarEventDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(event: CalendarEvent): Long

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(events: List<CalendarEvent>): List<Long>

    @Delete
    fun delete(event: CalendarEvent)

    @Query("delete from event where eid in (:eid)")
    fun deleteById(eid: Long)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(event: CalendarEvent)

    @Query("select * from event where eid in (:eid)")
    fun findById(eid: Long): CalendarEvent

    @Query("select * from event")
    fun findAll(): List<CalendarEvent>

    @Query("select * from event where creator_id in (:userId)")
    fun findByUserId(userId: Long): List<CalendarEvent>
}