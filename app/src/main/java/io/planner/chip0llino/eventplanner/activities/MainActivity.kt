package io.planner.chip0llino.eventplanner.activities

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import io.planner.chip0llino.eventplanner.R
import io.planner.chip0llino.eventplanner.controller.RequestControllerImpl
import io.planner.chip0llino.eventplanner.controller.UserCalendarDataControllerImpl
import io.planner.chip0llino.eventplanner.controller.interfaces.UserCalendarDataController
import io.planner.chip0llino.eventplanner.data.entities.CalendarEvent
import io.planner.chip0llino.eventplanner.fragments.CalendarEventListFragment
import io.planner.chip0llino.eventplanner.fragments.CalendarFragment
import io.planner.chip0llino.eventplanner.fragments.dummy.EventsContent
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import java.util.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, CalendarFragment.OnFragmentInteractionListener, CalendarEventListFragment.OnListFragmentInteractionListener {

    private lateinit var fragment: Fragment
    private lateinit var settings: SharedPreferences
    private var account: GoogleSignInAccount? = null
    private val TAG = "MainActivity"
    private var allEvents: List<CalendarEvent>? = null
    private var mGoogleSignInClient  : GoogleSignInClient? = null
    lateinit var userCreds : MutableSet<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        settings = getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE)

        setContentView(R.layout.activity_main)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        fragment = CalendarFragment.newInstance()
        supportFragmentManager.beginTransaction().replace(R.id.app_bar_main, fragment)
                .commit()

        /** initializing database */

        dataCtrl.initDb(applicationContext)
        reqCtrl.initQueue(this)
        loadEventsData()

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .build()

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
        userCreds = arrayListOf<String>().toMutableSet()
        userCreds = settings.getStringSet("Credentials", userCreds)
        token = settings.getString("token", "")

        account = GoogleSignIn.getLastSignedInAccount(applicationContext)

        when
        {
            userCreds.isNotEmpty() -> setUserData(userCreds)
            account != null || account?.photoUrl.toString() != "null" -> setUserData(account!!)
            else -> startLogin()
        }

        //settings.edit().putString(USER_ID, account!!.id).apply()
        registerUrl = getString(R.string.register_url)
        loginUrl = getString(R.string.login_url)
        serverUrl = getString(R.string.server_url)
    }

    private fun startLogin() {
        val login = Intent(this, LoginActivity::class.java)
        startActivityForResult(login, 1)
    }

    override fun getEventsFlowable(): List<CalendarEvent> {
        return this.allEvents!!
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        /**
         * FAB button action
         **/
        val fab = app_bar_main.findViewById<FloatingActionButton>(R.id.addNewEvent)
        fab.setOnClickListener(
                {
                    createEvent()
                })
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun loadEventsData() {
        try {
            val allEvents = dataCtrl.getEvents()
            EventsContent.COUNT = allEvents.size
            EventsContent.ITEMS = allEvents.toMutableList()
        }
        catch (e: Error)
        {
            Log.d(TAG, e.localizedMessage)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
// Handle navigation view item clicks here.

        when (item.itemId) {
            R.id.nav_calendar -> {
                if(fragment.activity != CalendarFragment::class.java)
                    fragment = CalendarFragment.newInstance()
            }
            R.id.nav_nearest -> {
                if(fragment.activity != CalendarEventListFragment::class.java)
                    fragment = CalendarEventListFragment.newInstance(1)
            }
            R.id.sign_out -> {
                signOut()
            }
        }

        supportFragmentManager.beginTransaction().replace(R.id.app_bar_main, fragment)
                .commit()

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun signOut() {
        mGoogleSignInClient?.signOut()
        userCreds.clear()
        startLogin()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        /** when authorizing */
        if(requestCode == 1 && resultCode == 1)
        {
            if(account != null) {
                try {
                    account = data!!.getParcelableExtra("user") as GoogleSignInAccount
                    setUserData(account!!)
                } catch (e: Exception) {
                    Log.e("ERR", e.localizedMessage)
                    val builder = AlertDialog.Builder(this)
                    builder.setMessage(e.localizedMessage).setTitle("Error").setCancelable(false)
                            .setNegativeButton("ОК",
                                    { dialogInterface: DialogInterface, _: Int -> dialogInterface.cancel() })
                            .create().show()
                }
            }
            else
            {
                if(token != "")
                {
                    userCreds = settings.getStringSet("Credentials", userCreds)
                    userCreds.add("")
                    settings.edit().putString("token", token).apply()
                }
                else
                {
                    startLogin()
                }
            }
        }
        /**
         * when creating event
         */
        if(requestCode == 2 && resultCode == Activity.RESULT_OK)
        {
            /**
             * getting edited event from intent
             */
            val event = data!!.getParcelableExtra<CalendarEvent>("eventRes")
            dataCtrl.add(event)
            reqCtrl.sendCreateEventRequest(event, userCreds.toList()[0], token, this)
        }
    }

    private fun setUserData(account: GoogleSignInAccount) {
        /**
         * sets user's profile's picture, name, last name and email
         */
        val uName = nav_view.getHeaderView(0).findViewById<TextView>(R.id.userName)
        val uEmail = nav_view.getHeaderView(0).findViewById<TextView>(R.id.userEmail)
        val uImg = nav_view.getHeaderView(0).findViewById<ImageView>(R.id.avatar)

        Glide.with(this.applicationContext).load(account.photoUrl).into(uImg)
        uName.text = account.displayName
        uEmail.text = account.email
    }

    private fun setUserData(userCreds: MutableSet<String>) {
        val uName = nav_view.getHeaderView(0).findViewById<TextView>(R.id.userName)
        val uEmail = nav_view.getHeaderView(0).findViewById<TextView>(R.id.userEmail)
        val uImg = nav_view.getHeaderView(0).findViewById<ImageView>(R.id.avatar)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Glide.with(this.applicationContext).load(getDrawable(R.mipmap.ic_launcher_round)).into(uImg)
        }

        uName.text = userCreds.toList()[0]
        //uEmail.text = userCreds.toList()[2]
    }

    /** Handler for showing events of the day which was selected */
    override fun daySelected() {
        if (fragment is CalendarFragment) {
            val calFrag = fragment as CalendarFragment
            calFrag.showDayEvents(dataCtrl, Calendar.getInstance().time)
        }
    }

    /** Creates new event and opens editor */
    private fun createEvent()
    {
        editEvent(CalendarEvent(123, "Event name", arrayOf(), arrayOf(), Calendar.getInstance().time, Calendar.getInstance().time))
    }

    /**
     * Opens event editor window
     */
    override fun editEvent(event: CalendarEvent)
    {
        val eventEditIntent = Intent(this, EventEdit::class.java)
        eventEditIntent.putExtra("event", event)
        startActivityForResult(eventEditIntent, 2)
    }

    /** returns database controller */
    override fun getDataCtrl(): UserCalendarDataController {
        return dataCtrl
    }

    companion object {
        val USER_ID = "userID"
        val FILE_NAME = "appconf"
        val dataCtrl = UserCalendarDataControllerImpl()
        val reqCtrl = RequestControllerImpl()
        var registerUrl = ""
        var loginUrl = ""
        var serverUrl = ""
        var token = ""
    }
}
