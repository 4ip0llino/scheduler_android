package io.planner.chip0llino.eventplanner.data.dao

import android.arch.persistence.room.*
import io.planner.chip0llino.eventplanner.data.entities.EventPoll

@Dao
interface EventPollDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(poll: EventPoll): Long

    @Delete
    fun delete(poll: EventPoll)

    @Update
    fun update(poll: EventPoll): Int

    @Query("select * from event_poll where epid in (:epid)")
    fun findById(epid: Long): EventPoll

    @Query("select * from event_poll where epid in (:epids)")
    fun findByIds(epids: List<Long>): List<EventPoll>

    @Query("UPDATE event_poll SET votes = :votes WHERE epid in (:pid)")
    fun updateVotes(pid: Long, votes: HashMap<Long, Long>)
}