package io.planner.chip0llino.eventplanner.fragments.dummy

import android.arch.lifecycle.LifecycleOwner
import android.content.Context
import io.planner.chip0llino.eventplanner.controller.UserCalendarDataControllerImpl
import io.planner.chip0llino.eventplanner.controller.interfaces.UserCalendarDataController
import io.planner.chip0llino.eventplanner.data.entities.CalendarEvent
import kotlinx.coroutines.experimental.CommonPool
import java.util.*

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 */
object EventsContent {

    /**
     * An array of sample (dummy) items.
     */
    //var ITEMS:MutableList<CalendarEvent> = ArrayList()
    var ITEMS:MutableList<CalendarEvent> = ArrayList()

    /**
     * A map of sample (dummy) items, by ID.
     */
    var ITEM_MAP:MutableMap<String, CalendarEvent> = HashMap()

    var COUNT = 0

    private fun addItem(item:CalendarEvent) {
        ITEMS.add(item)
        ITEM_MAP.put(item.eid.toString(), item)
    }

    private fun makeDetails(position:Int):String {
        val builder = StringBuilder()
        builder.append("Details about Item: ").append(position)
        for (i in 0 until position)
        {
            builder.append("\nMore details information here.")
        }
        return builder.toString()
    }
}
