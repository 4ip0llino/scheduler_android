package io.planner.chip0llino.eventplanner.fragments

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import io.planner.chip0llino.eventplanner.R
import io.planner.chip0llino.eventplanner.controller.interfaces.ListItemClickListener
import io.planner.chip0llino.eventplanner.data.entities.EventPoll
import io.planner.chip0llino.eventplanner.data.entities.User

class RecyclerListItemAdapter(private val mPolls: List<EventPoll>?, private val mAttendees: List<User>?, val fragment: EmptyList, val type: EmptyList.Companion.ListType, val listInteraction: EmptyList.ListFragmentInteractionListener): RecyclerView.Adapter<RecyclerListItemAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType:Int):ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder:ViewHolder, position:Int) {
        if(type == EmptyList.Companion.ListType.POLLS) {
            if(position == mPolls!!.size)
            {
                holder.mContentView.text = listInteraction.getTextAdd()
                holder.mIdView.text = "-1"
            }
            else {
                holder.mIdView.text = mPolls[position].epid.toString()
                holder.mContentView.text = mPolls[position].name
            }
        }
        else if(type == EmptyList.Companion.ListType.ATTENDEES)
        {
            if(position == mAttendees!!.size)
            {
                holder.mContentView.text = listInteraction.getTextAdd()
                holder.mPicture.setImageResource(android.R.drawable.ic_menu_my_calendar)
                holder.mIdView.text = "-1"
            }
            else {
                holder.mIdView.text = mAttendees[position].uid.toString()
                val fullName = mAttendees[position].name + " " + mAttendees[position].lastName
                holder.mContentView.text = fullName
                holder.mPicture.setImageResource(android.R.drawable.ic_menu_my_calendar)
            }
        }

        val listener: ListItemClickListener = object : ListItemClickListener
        {
            override fun onClick(view: View, position: Int, isLongClick: Boolean, eid: String) {
                if((!isLongClick)&&(eid == "-1")&&(type == EmptyList.Companion.ListType.POLLS))
                {
                    listInteraction.startNewPoll()
                }
                else if((!isLongClick)&&(type == EmptyList.Companion.ListType.POLLS))
                {
                    listInteraction.startEditPoll(eid.toLong())
                }
                else{
                    Toast.makeText(fragment.context, "Hello!", Toast.LENGTH_LONG).show()
                }
            }
        }
        holder.setListItemClickListener(listener)
    }

    override fun getItemCount():Int {

        return when (type)
        {
            EmptyList.Companion.ListType.ATTENDEES -> mAttendees!!.size + 1
            EmptyList.Companion.ListType.POLLS -> mPolls!!.size + 1
        }
    }

    inner class ViewHolder(mView: View) : RecyclerView.ViewHolder(mView), View.OnClickListener, View.OnLongClickListener {
        private var itemClickListener: ListItemClickListener? = null

        override fun onClick(p0: View?) {
            itemClickListener!!.onClick(p0!!, adapterPosition, false, mIdView.text.toString())
        }

        override fun onLongClick(p0: View?): Boolean {
            itemClickListener!!.onClick(p0!!, adapterPosition, true, mIdView.text.toString())
            return true
        }

        val mIdView: TextView = mView.findViewById(R.id.id_text) as TextView
        val mContentView: TextView = mView.findViewById(R.id.main_text) as TextView
        val mPicture: ImageView = mView.findViewById(R.id.item_image)

        override fun toString():String {
            return super.toString() + " '" + mContentView.text + "'"
        }

        fun setListItemClickListener(listener: ListItemClickListener)
        {
            this.itemClickListener = listener
        }

        init {
            mView.setOnClickListener(this)
            mView.setOnLongClickListener(this)
        }
    }
}