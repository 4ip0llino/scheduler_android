package io.planner.chip0llino.eventplanner.fragments

import android.app.DatePickerDialog
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.planner.chip0llino.eventplanner.R
import io.planner.chip0llino.eventplanner.data.entities.CalendarEvent
import kotlinx.android.synthetic.main.fragment_event_details.*
import java.text.SimpleDateFormat
import java.util.*


class MainDetailsFragment : Fragment()
{
    lateinit var event : CalendarEvent
    private lateinit var mListener : SaveEventDetails
    private val TAG = "MainDetailsFragment"

    override fun setArguments(args: Bundle?) {
        super.setArguments(args)
        event = args?.get("event") as CalendarEvent
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        try {
            mListener = context as SaveEventDetails
        }
        catch (e: Error)
        {
            Log.d(TAG, e.localizedMessage)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_event_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        eventDate.setOnClickListener { openDatePickerDiag() }
        eventDate.setOnFocusChangeListener { view1, _ -> if(view1 == eventDate) openDatePickerDiag() }

        eventName.setText(event.name)
        val fd = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
        eventDate.setText(fd.format(event.timeStart))
        val ft = SimpleDateFormat("hh:mm:ss", Locale.ENGLISH)
        eventTimeStart.setText(ft.format(event.timeStart))
    }

    /**
     * opens date picker dialog
     */
    private fun openDatePickerDiag() {
        val picker = DatePickerDialog(this.context, DatePickerDialog.OnDateSetListener { _, y, m, d ->  setDate(d, m, y)}, event.timeStart.year, event.timeStart.month, event.timeStart.day)
        picker.show()
    }

    /**
     * set event's start date
     */
    private fun setDate(d: Int, m: Int, y: Int)
    {   val sDate = String.format("%02d/%02d/%04d", d, m, y)
        eventDate.setText(sDate)
    }

    override fun onStop() {
        if(validate())
        {
            saveEvent()
            mListener.saveDetails(event)
        }
        super.onStop()
    }

    override fun onPause() {
        if(validate())
        {
            saveEvent()
            mListener.saveDetails(event)
        }
        super.onPause()
    }

     fun saveEvent() {
        event.name = eventName.text.toString()
        try {
            val f = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
            event.timeStart = f.parse(eventDate.text.toString())
        }
        catch (e:Error){
            Log.d(TAG, e.localizedMessage)
        }
        mListener.saveDetails(event)
    }

    /**
     * input validation
     */
    private fun validate(): Boolean
    {
        return (eventName.text.toString() != "")&&(eventDate.text.toString() != "")
    }

    /**
     * for interaction with main activity
     */
    interface SaveEventDetails
    {
        fun saveDetails(event: CalendarEvent)
    }
}