package io.planner.chip0llino.eventplanner.data.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import java.util.*

/**
 * Created by chip0llino on 27.01.2018.
 */
@Entity(tableName = "event")
class CalendarEvent(): Parcelable
{
        @PrimaryKey(autoGenerate = true)
        var eid: Long = 0
        @ColumnInfo(name = "creator_id")
        var creatorId: Long = 0
        @ColumnInfo(name = "name")
        var name: String = ""
        @ColumnInfo(name = "attendees")
        var attendees: Array<Long> = arrayOf()
        @ColumnInfo(name = "polls")
        var pollsIds: Array<Long> = arrayOf()
        @ColumnInfo(name = "date_start")
        var timeStart: Date = Calendar.getInstance().time
        @ColumnInfo(name = "date_end")
        var timeEnd: Date = Calendar.getInstance().time

        constructor(parcel: Parcel) : this() {
                eid = parcel.readLong()
                creatorId = parcel.readLong()
                name = parcel.readString()
                attendees = parcel.createLongArray().toTypedArray()
                pollsIds = parcel.createLongArray().toTypedArray()
                timeStart = Date(parcel.readLong())
                timeEnd = Date(parcel.readLong())
        }

        constructor(creatorId: Long, name: String, attendees: Array<Long>, pollsIds: Array<Long>, timeStart: Date, timeEnd: Date) : this()
        {
                this.creatorId = creatorId
                this.name = name
                this.attendees = attendees
                this.pollsIds = pollsIds
                this.timeStart = timeStart
                this.timeEnd = timeEnd
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
                parcel.writeLong(eid)
                parcel.writeLong(creatorId)
                parcel.writeString(name)
                parcel.writeLongArray(attendees.toLongArray())
                parcel.writeLongArray(pollsIds.toLongArray())
                parcel.writeLong(timeStart.time)
                parcel.writeLong(timeEnd.time)
        }

        override fun describeContents(): Int {
                return 0
        }

        companion object CREATOR : Parcelable.Creator<CalendarEvent> {
                override fun createFromParcel(parcel: Parcel): CalendarEvent {
                        return CalendarEvent(parcel)
                }

                override fun newArray(size: Int): Array<CalendarEvent?> {
                        return arrayOfNulls(size)
                }
        }
}