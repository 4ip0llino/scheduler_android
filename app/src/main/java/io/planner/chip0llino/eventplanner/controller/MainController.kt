package io.planner.chip0llino.eventplanner.controller

import com.google.api.services.calendar.CalendarScopes


/**
 * Created by chip0llino on 06.02.2018.
 * Controller for application. Helps to interact with data.
 */
class MainController
{
    companion object {

        val RC_SIGN_IN = 1

        private val SCOPES: Collection<String> =  mutableListOf(CalendarScopes.CALENDAR_READONLY)

        /*fun updateDatabase(context: Context) {
            val eventsDBhelper = EventsDatabase(context)
            val eventsDB = eventsDBhelper.writableDatabase

            val values = ContentValues().apply {
                put(EventsDatabase.DatabaseTables.Users.COLUMN_EMAIL, "admin@admin.com")
                put(EventsDatabase.DatabaseTables.Users.COLUMN_PASSWORD, "123456")
                put(EventsDatabase.DatabaseTables.Users.COLUMN_NAME, "Admin")
            }

            eventsDB.insert(EventsDatabase.DatabaseTables.Users.TABLE_NAME, null, values)

            Toast.makeText(context, "Data inserted successfully!", Toast.LENGTH_LONG).show()
        }*/

        /**
         * Check Credentials from db
         */
        /*fun checkCredentials(context: Context, mEmail: String, mPassword: String) : Boolean {
            val eventsDBhelper = EventsDatabase(context)
            val eventsDB = eventsDBhelper.readableDatabase
            var exists = false

            val cursor = eventsDB.rawQuery("SELECT ${EventsDatabase.DatabaseTables.Users.COLUMN_EMAIL}, ${EventsDatabase.DatabaseTables.Users.COLUMN_PASSWORD}, ${EventsDatabase.DatabaseTables.Users.COLUMN_NAME} FROM ${EventsDatabase.DatabaseTables.Users.TABLE_NAME}", null)

            while (cursor.moveToNext()) {
                val email = cursor.getString(0)
                val pass = cursor.getString(1)

                if ((email == mEmail)&&(pass == mPassword)){
                    exists = true
                    LoginActivity.USER_EMAIL = email
                    LoginActivity.USER_NAME = cursor.getString(2)
                    break
                }
            }
            cursor.close()

            return exists
        }*/
    }
}