package io.planner.chip0llino.eventplanner.controller

import android.content.Context
import android.util.Log
import android.widget.Toast
import io.planner.chip0llino.eventplanner.activities.MainActivity
import io.planner.chip0llino.eventplanner.controller.interfaces.LoginController

/**
 * Created by chip0llino on 23.03.2018.
 *
 * Login controller implementation
 * Place all login logic here
 */
class LoginControllerImpl : LoginController {

    val LOG_TAG = "LoginController"
    private var mLoggedIn = false
    private var mError = false
    private var mNetError = false
    private var mErrorCode = 0
    private var mErrorMessage = ""

    override var loggedIn: Boolean
        get() = mLoggedIn
        set(value) { mLoggedIn = value }

    override var error: Boolean
        get() = mError
        set(value) { mError = value }

    override fun showLoginError(ctx: Context) {

        var msg = if(!mNetError) {
            when (mErrorCode) {
                10 -> "SHA1 cert is outdated"
                12500 -> "Sign in failed, please, try to update Google Services"
                12501 -> "Sign in cancelled"
                else -> "Unknown error"
            }
        }
        else
        {
            when (mErrorCode) {
                0 -> "Connection error"
                404 -> "Not found"
                401 -> "Invalid credentials"
                else -> "Unknown error"
            }
        }

        if(mErrorMessage != "") { msg = mErrorMessage }

        Toast.makeText(ctx, "Error: $msg\nCode: $mErrorCode", Toast.LENGTH_LONG).show()
        Log.e(LOG_TAG, msg)
    }

    override fun setNetworkErrorCode(errCode: Int) {
        mNetError = true
        mErrorCode = errCode
    }

    override fun checkUser(email: String): Boolean {
        return MainActivity.dataCtrl.getUserByEmail(email) != null
    }

    override fun login(username: String, password: String) {
        MainActivity.reqCtrl.sendLoginRequest(username, password, this)
    }

    override fun setSuccess() {
        error = false
        loggedIn = true
    }

    override fun setError() {
        error = true
        loggedIn = false
    }

    override fun setErrorMessage(message: String)
    {
        mErrorMessage = message
    }

    override fun setErrorCode(errCode: Int) {
        mErrorCode = errCode
    }
}