package io.planner.chip0llino.eventplanner.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.applandeo.materialcalendarview.EventDay
import io.planner.chip0llino.eventplanner.R
import io.planner.chip0llino.eventplanner.controller.interfaces.UserCalendarDataController
import io.planner.chip0llino.eventplanner.fragments.dummy.EventsContent
import kotlinx.android.synthetic.main.fragment_calendar.*
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [CalendarFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [CalendarFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class CalendarFragment : Fragment() {

    private var mListener: OnFragmentInteractionListener? = null
    //private val ctrlCal = CalendarControllerImpl()
    private val TAG = "CalendarFragmnet"

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        try {
            calendarPicker.setOnClickListener({ mListener!!.daySelected() })
            //ctrlCal.showAllEvents(mListener!!.getDataCtrl(), calendarPicker)
            mListener?.loadEventsData()
            setDate(Date(System.currentTimeMillis()))
            val events = EventsContent.ITEMS
            val dayEvents: MutableList<EventDay> = ArrayList()

            for (ev in events)
            {
                val calendarDate = Calendar.getInstance()
                calendarDate.time = ev.timeStart
                dayEvents.add(EventDay(calendarDate))
            }

            calendarPicker.setEvents(dayEvents)
        }
        catch (e: Exception)
        {
            Toast.makeText(context, e.localizedMessage, Toast.LENGTH_LONG).show()
            Log.e(TAG, e.localizedMessage)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_calendar, container, false)
    }

    /** Checks interface implementation */
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /** Set date for calendar (for external classes) */
    private fun setDate(date: Date)
    {
        calendarPicker.setDate(date)
    }

    fun showDayEvents(ctrl: UserCalendarDataController, day: Date)
    {
        //ctrlCal.showDayEvents(ctrl, day)
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnFragmentInteractionListener {
        fun daySelected()
        fun getDataCtrl(): UserCalendarDataController
        fun loadEventsData()
    }

    companion object {
        
        fun newInstance(): CalendarFragment {
            return CalendarFragment()
        }
    }
}// Required empty public constructor
