package io.planner.chip0llino.eventplanner.data.dao

import android.arch.persistence.room.*
import io.planner.chip0llino.eventplanner.data.entities.User

@Dao
interface UserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(users: List<User>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(users: User): Long

    @Update
    fun update(user: User)

    @Delete
    fun delete(user: User)

    @Query("SELECT * FROM user WHERE uid IN (:userId)")
    fun findById(userId: Long): User?

    @Query("SELECT * FROM user WHERE uid IN (:users)")
    fun findByIds(users: List<Long>): List<User>

    @Query("SELECT * FROM user WHERE email IN (:email)")
    fun findByEmail(email: String): User?

    @Query("SELECT * FROM user")
    fun selectAll(): List<User>
}