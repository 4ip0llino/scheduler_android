package io.planner.chip0llino.eventplanner.controller

import android.content.Context
import android.util.Log
import io.planner.chip0llino.eventplanner.R
import io.planner.chip0llino.eventplanner.activities.MainActivity
import io.planner.chip0llino.eventplanner.controller.interfaces.RegistrationController
import io.planner.chip0llino.eventplanner.data.entities.User

class RegistrationControllerImpl: RegistrationController
{
    private val TAG = "RegistrationController"

    override fun addUser(user: User): Boolean {
        return try {
            MainActivity.reqCtrl.sendRegisterRequest(user.login, user.email, user.password)
            MainActivity.dataCtrl.add(user)
            true
        }
        catch (e: Error)
        {
            Log.e(TAG, e.localizedMessage)
            false
        }
    }

    override fun validateInput(login: String, name: String, lastName: String, email: String, password: String, passwordRepeat: String, date: String, context: Context): String {
        return when {
            login == "" -> context.getString(R.string.msg_login_empty)
            name == "" -> context.getString(R.string.msg_empty_name)
            lastName == "" -> context.getString(R.string.msg_empty_lastName)
            email == "" -> context.getString(R.string.msg_empty_email)
            !email.contains('@') -> context.getString(R.string.msg_incorrect_email)
            date == "" -> context.getString(R.string.msg_date_empty)
            password == "" -> context.getString(R.string.msg_empty_password)
            password.length < 8 -> context.getString(R.string.msg_short_password)
            password != passwordRepeat -> context.getString(R.string.msg_password_not_equal)
            name.contains(Regex("[!@#%^&*()\\-+=±`~.,\\[\\]\\{\\}\\<\\>]")) -> context.getString(R.string.msg_name_illegal)
            lastName.contains(Regex("[!@#%^&*()\\-+=±`~.,\\[\\]\\{\\}\\<\\>]")) -> context.getString(R.string.msg_illegal_lastname)
            MainActivity.dataCtrl.getUserByEmail(email) != null -> context.getString(R.string.msg_email_exists)
            else -> ""
        }
    }
}