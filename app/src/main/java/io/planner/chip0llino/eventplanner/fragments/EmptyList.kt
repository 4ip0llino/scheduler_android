package io.planner.chip0llino.eventplanner.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.planner.chip0llino.eventplanner.R
import io.planner.chip0llino.eventplanner.activities.MainActivity
import io.planner.chip0llino.eventplanner.data.entities.CalendarEvent
import io.planner.chip0llino.eventplanner.data.entities.EventPoll
import io.planner.chip0llino.eventplanner.data.entities.User
import java.io.Serializable

class EmptyList : Fragment() {

    private var event : CalendarEvent? = null
    private var type : ListType? = null
    private lateinit var mListener : ListFragmentInteractionListener
    private var mMap : HashMap<Long, String> = HashMap()
    private var attendees : List<User> = arrayListOf()
    private var polls : List<EventPoll> = arrayListOf()

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if(context is ListFragmentInteractionListener)
        {
           mListener = context
        }
        else
        {
            throw Error("Activity using this fragment must implement ListFragmentInteractionListener!")
        }
    }

    override fun setArguments(args: Bundle?) {
        super.setArguments(args)
        event = args?.get("event") as CalendarEvent
        type = args.get("listType") as ListType
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_empty, container, false)

        // Set the adapter
        if (view is RecyclerView) {
            val context = view.getContext()
            view.layoutManager = LinearLayoutManager(context)
            when (type)
            {
                ListType.ATTENDEES -> view.adapter = RecyclerListItemAdapter(null, attendees, this, ListType.ATTENDEES, mListener)
                ListType.POLLS -> view.adapter = RecyclerListItemAdapter(polls, null, this, ListType.POLLS, mListener)
            }
        }

        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if((event != null)&&(event?.attendees != null))
        {
            if(type == ListType.ATTENDEES) {
                attendees = MainActivity.dataCtrl.getUsersByIds(event!!.attendees.toList())
                for (attendee in attendees) {
                    mMap[attendee.uid] = "${attendee.name} ${attendee.lastName}"
                }
            }
            else
            {
                polls = MainActivity.dataCtrl.getPollsByIds(event!!.pollsIds.toList())

                for (poll in polls) {
                    mMap[poll.epid] = poll.name
                }
            }
        }
    }

    companion object {
        enum class ListType : Serializable {
            ATTENDEES,
            POLLS
        }
    }

    interface ListFragmentInteractionListener
    {
        fun startEditPoll(pollId: Long)
        fun startNewPoll()
        fun getTextAdd():String
    }
}