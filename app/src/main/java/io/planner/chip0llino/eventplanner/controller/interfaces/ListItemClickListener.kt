package io.planner.chip0llino.eventplanner.controller.interfaces

import android.view.View

interface ListItemClickListener {
    fun onClick(view: View, position: Int, isLongClick: Boolean, eid: String)
}