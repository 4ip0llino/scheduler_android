package io.planner.chip0llino.eventplanner.controller.interfaces

import android.view.View
import java.util.*

interface CalendarController
{
    fun showAllEvents(ctrl: UserCalendarDataController, calendarPicker: View)

    fun deleteEvents()

    fun showNewEvent()

    fun showDayEvents(ctrl: UserCalendarDataController, day: Date)
}