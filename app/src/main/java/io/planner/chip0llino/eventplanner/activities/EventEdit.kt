package io.planner.chip0llino.eventplanner.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.ViewGroup
import io.planner.chip0llino.eventplanner.R
import io.planner.chip0llino.eventplanner.activities.EventEdit.Companion.tab1
import io.planner.chip0llino.eventplanner.activities.EventEdit.Companion.tab2
import io.planner.chip0llino.eventplanner.activities.EventEdit.Companion.tab3
import io.planner.chip0llino.eventplanner.data.entities.CalendarEvent
import io.planner.chip0llino.eventplanner.fragments.EmptyList
import io.planner.chip0llino.eventplanner.fragments.MainDetailsFragment
import kotlinx.android.synthetic.main.activity_event_edit.*
import kotlinx.android.synthetic.main.fragment_event_details.*


class EventEdit : AppCompatActivity(), MainDetailsFragment.SaveEventDetails, io.planner.chip0llino.eventplanner.fragments.EmptyList.ListFragmentInteractionListener {

    private val TAG = "EventEdit"
    lateinit var event : CalendarEvent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_edit)

        try {
            tab1 = resources.getText(R.string.event_main).toString()
            tab2 = resources.getText(R.string.event_attendees).toString()
            tab3 = resources.getText(R.string.event_polls).toString()

            event = intent.getParcelableExtra("event")
            viewPager.adapter =  TabLayoutAdapter(supportFragmentManager, event)
            tabLayout.addTab(tabLayout.newTab().setText(resources.getText(R.string.event_main)))
            tabLayout.addTab(tabLayout.newTab().setText(resources.getText(R.string.event_attendees)))
            tabLayout.addTab(tabLayout.newTab().setText(resources.getText(R.string.event_polls)))
            tabLayout.setupWithViewPager(viewPager)
        }
        catch (e : Error)
        {
            Log.d(TAG, e.localizedMessage)
        }
    }

    override fun onResume() {
        super.onResume()
        tabLayout.setupWithViewPager(viewPager)
    }

    /**
     * opens poll for voting
     */
    override fun startEditPoll(pollId: Long)
    {
        val poll = Intent(this, PollActivity::class.java)
        poll.putExtra("poll_id", pollId)
        startActivity(poll)
    }

    /**
     * creates new poll
     */
    override fun startNewPoll() {
        val poll = Intent(this, PollActivity::class.java)
        poll.putExtra("isEditable", true)
        startActivityForResult(poll, 1)
    }

    /**
     * returns text string
     */
    override fun getTextAdd(): String {
        return getText(R.string.str_add).toString()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if((requestCode == 1)&&(resultCode == 1))
        {
            val pid = data!!.getLongExtra("poll_id", -1L)

            if(!event.pollsIds.contains(pid)&&(pid != -1L))
            {
                event.pollsIds = event.pollsIds.plus(pid)
            }
        }
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        btn_save.setOnClickListener {
            resultOK()
        }

        btn_cancel.setOnClickListener {
            resultCancel()
        }
    }

    override fun onBackPressed() {
        resultCancel()
        finish()
    }

    private fun resultOK()
    {
        if(validate()) {
            val adapter = viewPager.adapter as TabLayoutAdapter
            val details = adapter.getFragment(0) as MainDetailsFragment
            details.saveEvent()
            val result = Intent()
            result.putExtra("eventRes", event)
            setResult(Activity.RESULT_OK, result)
            finish()
        }
    }

    private fun resultCancel()
    {
        setResult(Activity.RESULT_CANCELED)
        finish()
    }

    override fun saveDetails(event: CalendarEvent) {
        this.event.timeStart = event.timeStart
        this.event.name = event.name
    }

    /**
     * input validation
     */
    private fun validate(): Boolean
    {
        val adapter = viewPager.adapter as TabLayoutAdapter
        val details = adapter.getFragment(0) as MainDetailsFragment
        return (details.eventName.text.toString() != "")&&(details.eventDate.text.toString() != "")
    }

    companion object {
        var tab1 = ""
        var tab2 = ""
        var tab3 = ""
    }
}

class TabLayoutAdapter(fm: FragmentManager?, val event: CalendarEvent) : FragmentPagerAdapter(fm)
{
    private var mPageReferenceMap: HashMap<Int, Fragment> = HashMap()

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val fragment = super.instantiateItem(container, position) as Fragment
        mPageReferenceMap[position] = fragment
        return fragment
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        super.destroyItem(container, position, `object`)
        mPageReferenceMap.remove(position)
    }

    fun getFragment(key: Int): Fragment? {
        return mPageReferenceMap[key]
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> tab1
            1 -> tab2
            2 -> tab3
            else -> ""
        }
    }

    override fun getItem(position: Int): Fragment {

        val args = Bundle()

        val fragment: Fragment? = when (position)
        {
            0 -> MainDetailsFragment()
            1 -> EmptyList()
            2 -> EmptyList()
            else -> null
        }

        when (position)
        {
            1 -> args.putSerializable("listType", EmptyList.Companion.ListType.ATTENDEES)
            2 -> args.putSerializable("listType", EmptyList.Companion.ListType.POLLS)
        }

        args.putParcelable("event", event)
        fragment!!.arguments = args
        return fragment
    }

    private val COUNT: Int = 3

    override fun getCount(): Int {
        return COUNT
    }
}
