package io.planner.chip0llino.eventplanner.controller.interfaces

import android.content.Context
import io.planner.chip0llino.eventplanner.data.EventsDatabase
import io.planner.chip0llino.eventplanner.data.entities.CalendarEvent
import io.planner.chip0llino.eventplanner.data.entities.EventPoll
import io.planner.chip0llino.eventplanner.data.entities.User

interface UserCalendarDataController {
    fun getEvents(): List<CalendarEvent>
    fun getEventById(eid: Long): CalendarEvent
    fun update(event: CalendarEvent)
    fun update(user: User)
    fun update(poll: EventPoll): Int
    /*fun updatePollVotes(pid: Long, votes: HashMap<Long, Long>)*/
    fun initDb(ctx: Context): EventsDatabase
    fun getUserById(uid: Long): User?
    fun closeDb()
    fun getUsersByIds(users: List<Long>): List<User>
    fun getPollsByIds(polls: List<Long>): List<EventPoll>
    fun getUserByEmail(email: String): User?
    fun add(event: CalendarEvent): Long
    fun add(user: User): Long
    fun add(poll: EventPoll): Long
    fun getPollById(pollId: Long): EventPoll
}