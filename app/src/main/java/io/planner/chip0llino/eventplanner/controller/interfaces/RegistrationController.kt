package io.planner.chip0llino.eventplanner.controller.interfaces

import android.content.Context
import io.planner.chip0llino.eventplanner.data.entities.User

interface RegistrationController {
    fun addUser(user: User): Boolean
    fun validateInput(login: String, name: String, lastName: String, email: String, password: String, passwordRepeat: String, date: String, context: Context): String
}