package io.planner.chip0llino.eventplanner.activities

import android.app.DatePickerDialog
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import io.planner.chip0llino.eventplanner.R
import io.planner.chip0llino.eventplanner.controller.RegistrationControllerImpl
import io.planner.chip0llino.eventplanner.data.entities.User
import kotlinx.android.synthetic.main.activity_registration.*
import java.text.SimpleDateFormat
import java.util.*

class RegistrationActivity : AppCompatActivity() {
    private val registrationController = RegistrationControllerImpl()
    private val TAG = "RegistrationActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)

        btn_reg.setOnClickListener { registerUser(this) }
        btn_canc.setOnClickListener { finish() }
        field_bdate.setOnFocusChangeListener { view, _ -> if(view == field_bdate)  { openDatePicker() } }
        field_bdate.setOnClickListener { openDatePicker() }
    }

    private fun openDatePicker() {
        val listener = DatePickerDialog.OnDateSetListener { _, y, m, d ->  field_bdate.setText("$d/$m/$y") }
        val calDate = Calendar.getInstance().time
        val picker = DatePickerDialog(this, listener, calDate.year, calDate.month, calDate.day)
        picker.show()
    }

    private fun registerUser(context: Context) {
        val login = field_login.text.toString()
        val name = field_name.text.toString()
        val lastName = field_last_name.text.toString()
        val email = field_email.text.toString()
        val password = field_password.text.toString()
        val passRepeat = field_password_repeat.text.toString()
        val bDate = field_bdate.text.toString()

        val msg = registrationController.validateInput(login, name, lastName, email, password, passRepeat, bDate, this)
        if (msg == "")
        {
            try {
                val fd = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
                val parsedDate = fd.parse(bDate)
                val newUser = User(0, login, name, password, lastName, email, parsedDate)
                val success = registrationController.addUser(newUser)

                if(success)
                {
                    finish()
                }
                else
                {
                    Toast.makeText(context, "Error happened when registering user", Toast.LENGTH_LONG).show()
                }
            }
            catch (e: Error)
            {
                Log.e(TAG, e.localizedMessage)
            }
        }
        else
        {
            Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
        }
    }
}
