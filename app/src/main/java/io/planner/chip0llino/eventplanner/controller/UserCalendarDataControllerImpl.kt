package io.planner.chip0llino.eventplanner.controller

import android.content.Context
import io.planner.chip0llino.eventplanner.controller.interfaces.UserCalendarDataController
import io.planner.chip0llino.eventplanner.data.EventsDatabase
import io.planner.chip0llino.eventplanner.data.EventsDatabase.Companion.getInstance
import io.planner.chip0llino.eventplanner.data.entities.CalendarEvent
import io.planner.chip0llino.eventplanner.data.entities.EventPoll
import io.planner.chip0llino.eventplanner.data.entities.User
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.runBlocking
import java.util.*

/**
 * Created by chip0llino on 27.01.2018.
 */
class UserCalendarDataControllerImpl: UserCalendarDataController {

    override fun initDb(ctx: Context): EventsDatabase {

        val asyncInit = async(CommonPool)
        {
            appDb = getInstance(ctx)
            //addTestData()
        }
        runBlocking { asyncInit.await() }
        return appDb!!
    }

    private fun addTestData() {

        val user1 = User()
        user1.email = "test@testing.com"
        user1.name = "Kirill"
        user1.lastName = "Lukichev"
        val dob = Calendar.getInstance()
        dob.set(1996, 4, 4)
        user1.birthday = dob.time
        val userId = add(user1)
        val user2 = User()
        user2.email = "email"
        user2.name = "Alexander"
        user2.lastName = "Barskov"
        val userId2 = add(user2)
        val user3 = User()
        user3.email = "email"
        user3.name = "Artem"
        user3.lastName = "Kravchenko"
        val userId3 = add(user3)
        val attend: Array<Long> = arrayOf(userId, userId2, userId3)
        val polls: Array<Long> = arrayOf()
        val newEvent = CalendarEvent(1, "Diplom", attend, polls, dob.time, dob.time)
        val eventId = add(newEvent)
        addPolls(userId, eventId)
    }

    /** adds event to database */
    override fun add(event: CalendarEvent): Long {
        return appDb!!.eventDao().insert(event)
    }

    /** adds user to database */
    override fun add(user: User): Long {
        return appDb!!.userDao().insert(user)
    }

    /** adds poll to database */
    override fun add(poll: EventPoll): Long {
        return appDb!!.pollDao().insert(poll)
    }

    /** returns all events from database */
    override fun getEvents(): List<CalendarEvent> {
        return appDb!!.eventDao().findAll()
    }

    /**
     * updates calendar event in db
     */
    override fun update(event: CalendarEvent) {
        appDb!!.eventDao().update(event)
    }

    /**
     * updates user in db
     */
    override fun update(user: User) {
        appDb!!.userDao().update(user)
    }

    /**
     * updates poll in db
     */
    override fun update(poll: EventPoll): Int {
        return appDb!!.pollDao().update(poll)
    }

    /*override fun updatePollVotes(pid: Long, votes: HashMap<Long, Long>) {
        return appDb!!.pollDao().updateVotes(pid, votes)
    }*/

    /** returns user from database by id */
    override fun getUserById(uid: Long): User? {
        var user: User? = null
        val asyncGetUser = async(CommonPool) { user = appDb!!.userDao().findById(uid) }
        runBlocking { asyncGetUser.await() }
        return user
    }

    private fun addPolls(userId: Long, eventId: Long)
    {
        val eid = appDb!!.eventDao().findById(eventId)
        val poll1 = EventPoll(1, userId, "Choose date")
        poll1.answers[1] = "6.05.2018"
        poll1.answers[2] = "25.11.2018"
        poll1.answers[3] = "17.08.2018"
        val pollId1 = add(poll1)
        val poll2 = EventPoll(2, userId, "Choose place")
        poll2.answers[1] = "Cafe"
        poll2.answers[2] = "Home"
        poll2.answers[3] = "Pizzeria"
        poll2.answers[4] = "Cottage"
        val pollId2 = add(poll2)
        val poll3 = EventPoll(3, userId, "Wanna play some games on celebration?")
        poll3.answers[1] = "Yes :)"
        poll3.answers[2] = "No :("
        val pollId3 = add(poll3)
        val poll4 = EventPoll(4, userId, "Choose birthday cake :)")
        poll4.answers[1] = "Brownie"
        poll4.answers[2] = "Biscuit"
        poll4.answers[3] = "Cheesecake"
        poll4.answers[4] = "Cookie"
        poll4.answers[5] = "Cupcake"
        poll4.answers[6] = "Fruitcake"
        val pollId4 = add(poll4)
        val polls : Array<Long> = arrayOf(pollId1, pollId2, pollId3, pollId4)
        eid.pollsIds = polls
        appDb!!.eventDao().update(eid)
    }

    /**
     * get all users by ids
     */
    override fun getUsersByIds(users: List<Long>): List<User> {
        return appDb!!.userDao().findByIds(users)
    }

    override fun getEventById(eid: Long): CalendarEvent {
        return appDb!!.eventDao().findById(eid)
    }

    override fun closeDb() {
        EventsDatabase.destroyInstance()
    }

    override fun getPollsByIds(polls: List<Long>): List<EventPoll> {
        return appDb!!.pollDao().findByIds(polls)
    }

    override fun getPollById(pollId: Long): EventPoll {
        return appDb!!.pollDao().findById(pollId)
    }

    override fun getUserByEmail(email: String): User? {
        return appDb!!.userDao().findByEmail(email)
    }

    companion object
    {
        var appDb: EventsDatabase? = null
    }
}