package io.planner.chip0llino.eventplanner.data.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "event_poll")
class EventPoll
(
    @PrimaryKey(autoGenerate = true)
    var epid: Long,

    @ColumnInfo(name = "creator_id")
    var creatorId: Long,

    @ColumnInfo(name = "name")
    var name: String,

    @ColumnInfo(name = "answers")
    var answers: HashMap<Long, String> = HashMap(),

    @ColumnInfo(name = "votes")
    var votes: HashMap<Long, Long> = HashMap()
)

