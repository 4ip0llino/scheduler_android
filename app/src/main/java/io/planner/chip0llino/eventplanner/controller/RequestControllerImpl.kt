package io.planner.chip0llino.eventplanner.controller

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley.newRequestQueue
import io.planner.chip0llino.eventplanner.activities.MainActivity
import io.planner.chip0llino.eventplanner.controller.interfaces.LoginController
import io.planner.chip0llino.eventplanner.controller.interfaces.RequestController
import io.planner.chip0llino.eventplanner.data.entities.CalendarEvent
import org.json.JSONObject
import java.util.*

class RequestControllerImpl: RequestController {
    private val TAG = "RequestControllerImpl"

    override fun initQueue(context: Context) {
        mRequestQueue = newRequestQueue(context)
    }

    private var mErrorCode = 0
    private var mErrorMessage = ""

    override fun showLoginError(ctx: Context) {

        var msg =
                when (mErrorCode) {
                    0 -> "Connection error"
                    1 -> "Error while adding event"
                    404 -> "Not found"
                    401 -> "Not authorized"
                    else -> "Unknown error"
                }

        if((mErrorMessage != "")&&(msg == "Unknown error")) { msg = mErrorMessage }

        Toast.makeText(ctx, "Error: $msg\nCode: $mErrorCode", Toast.LENGTH_LONG).show()
        Log.e(TAG, msg)
    }

    override fun sendRegisterRequest(username: String, email: String, password: String) {
        val url = MainActivity.registerUrl
        val data = hashMapOf(Pair("username",username), Pair("password",password), Pair("email",email))
        val request = object : JsonObjectRequest(Request.Method.POST, url, JSONObject(data),
                Response.Listener {
                    Log.d(TAG, it.toString(1))
                },
                Response.ErrorListener {
                    if(it.localizedMessage != null)
                    {
                        Log.e(TAG, it.localizedMessage)
                    }
                })
        {
        }
        Log.d(TAG, request.body?.contentToString())
        mRequestQueue!!.add(request)
    }

    override fun sendLoginRequest(username: String, password: String, controller: LoginController) {
        val url = MainActivity.loginUrl
        val data = hashMapOf(Pair("username",username), Pair("password",password))
        val request = object : JsonObjectRequest(Request.Method.POST, url, JSONObject(data),
                Response.Listener {
                    Log.d(TAG, it.toString(1))
                    MainActivity.token = it.get("token") as String
                    controller.setSuccess()
                },
                Response.ErrorListener {
                    controller.setError()
                    if(it.networkResponse != null)
                    {
                        controller.setNetworkErrorCode(it.networkResponse.statusCode)
                    }
                    if(it.localizedMessage != null)
                    {
                        Log.e(TAG, it.localizedMessage)
                    }
                }) {}
        Log.d(TAG, request.body?.contentToString())
        mRequestQueue!!.add(request)
    }

    override fun sendCreateEventRequest(event: CalendarEvent, username: String, token: String, context: Context) {
        /*val url = "${MainActivity.serverUrl}/event"
        val fd = SimpleDateFormat("yyyy-MM-dd hh:mm", Locale.ENGLISH)
        val data = hashMapOf(Pair("eventname", event.name), Pair("initiator", username), Pair("date_from", fd.format(event.timeStart)), Pair("date_to", fd.format(event.timeEnd)), Pair("description", ""))
        val request = object : JsonObjectRequest(Request.Method.POST, url, JSONObject(data),
                Response.Listener {
                    Log.d(TAG, it.toString(1))
                    if(it.getString("message") != "Success")
                    {
                        mErrorCode = 1
                        showLoginError(context)
                    }
                },
                Response.ErrorListener {
                    if(it.networkResponse != null)
                    {
                        mErrorCode = it.networkResponse.statusCode
                    }
                    if(it.localizedMessage != null)
                    {
                        mErrorMessage = it.localizedMessage
                        Log.e(TAG, it.localizedMessage)
                    }
                })
        {
            override fun getHeaders(): MutableMap<String, String> {
                val customHeaders =  mapOf<String, String>().toMutableMap()
                customHeaders["Content-Type"] = "[{\"key\":\"Content-Type\",\"value\":\"application/json\"}]"
                customHeaders["Authorization: Bearer"] = MainActivity.token
                return customHeaders
            }
        }
        Log.d(TAG, request.body?.contentToString())
        mRequestQueue!!.add(request)*/
    }

    override fun sendChangeEventRequest(event: CalendarEvent, token: String, context: Context) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun sendDeleteEventRequest(event: CalendarEvent, token: String, context: Context) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun sendGetEventDetailsRequest(event: CalendarEvent, token: String, context: Context) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun sendInviteRequest(eventname: String, username: String, context: Context) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun sendGetEventsForUser(username: String, date: Date, days: Int, context: Context) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object {
        var mRequestQueue: RequestQueue? = null
    }
}