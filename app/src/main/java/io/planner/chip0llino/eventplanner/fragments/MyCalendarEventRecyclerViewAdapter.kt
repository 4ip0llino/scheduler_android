package io.planner.chip0llino.eventplanner.fragments

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import io.planner.chip0llino.eventplanner.R
import io.planner.chip0llino.eventplanner.activities.MainActivity
import io.planner.chip0llino.eventplanner.controller.interfaces.ListItemClickListener
import io.planner.chip0llino.eventplanner.data.entities.CalendarEvent
import io.planner.chip0llino.eventplanner.fragments.CalendarEventListFragment.OnListFragmentInteractionListener

/**
 * [RecyclerView.Adapter] that can display a [DummyItem] and makes a call to the
 * specified [OnListFragmentInteractionListener].
 */
class MyCalendarEventRecyclerViewAdapter(private val mValues: List<CalendarEvent>, private val mListener: OnListFragmentInteractionListener?, val context: CalendarEventListFragment):RecyclerView.Adapter<MyCalendarEventRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent:ViewGroup, viewType:Int):ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.event_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder:ViewHolder, position:Int) {
        holder.mItem = mValues[position]
        holder.mIdView.text = mValues[position].name
        holder.mContentView.text = mValues[position].timeStart.toString()
        holder.mEidView.text = mValues[position].eid.toString()

        val listener: ListItemClickListener = object : ListItemClickListener
        {
            override fun onClick(view: View, position: Int, isLongClick: Boolean, eid: String) {
                if(!isLongClick)
                {
                    editEvent(eid.toLong())
                }
            }
        }
        holder.setListItemClickListener(listener)
    }

    private fun editEvent(eid: Long) {
        val eventToEdit = MainActivity.dataCtrl.getEventById(eid)
        mListener?.editEvent(eventToEdit)
    }

    override fun getItemCount():Int {
        return mValues.size
    }

    inner class ViewHolder(mView: View) : RecyclerView.ViewHolder(mView), View.OnClickListener, View.OnLongClickListener {
        var txtEid : TextView = mView.findViewById(R.id.eid_label)
        private var itemClickListener: ListItemClickListener? = null

        override fun onClick(p0: View?) {
            itemClickListener!!.onClick(p0!!, adapterPosition, false, txtEid.text.toString())
        }

        override fun onLongClick(p0: View?): Boolean {
            itemClickListener!!.onClick(p0!!, adapterPosition, true, txtEid.text.toString())
            return true
        }

        val mIdView:TextView = mView.findViewById(R.id.name) as TextView
        val mContentView:TextView = mView.findViewById(R.id.date) as TextView
        val mEidView: TextView = mView.findViewById(R.id.eid_label) as TextView
        var mItem: CalendarEvent? = null

        override fun toString():String {
            return super.toString() + " '" + mContentView.text + "'"
        }

        fun setListItemClickListener(listener: ListItemClickListener)
        {
            this.itemClickListener = listener
        }

        init {
            mView.setOnClickListener(this)
            mView.setOnLongClickListener(this)
        }
    }
}
