package io.planner.chip0llino.eventplanner.controller.interfaces

import android.content.Context

/**
 * Created by chip0llino on 23.03.2018.
 */
interface LoginController {
    var loggedIn: Boolean
    var error: Boolean

    fun showLoginError(ctx: Context)
    fun checkUser(email: String): Boolean
    fun login(username: String, password: String)
    fun setSuccess()
    fun setError()
    fun setErrorCode(errCode: Int)
    fun setErrorMessage(message: String)
    fun setNetworkErrorCode(errCode: Int)
}