package io.planner.chip0llino.eventplanner.data

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.content.Context
import io.planner.chip0llino.eventplanner.data.EventsDatabase.Companion.DATABASE_VERSION
import io.planner.chip0llino.eventplanner.data.dao.CalendarEventDao
import io.planner.chip0llino.eventplanner.data.dao.EventPollDao
import io.planner.chip0llino.eventplanner.data.dao.UserDao
import io.planner.chip0llino.eventplanner.data.entities.CalendarEvent
import io.planner.chip0llino.eventplanner.data.entities.EventPoll
import io.planner.chip0llino.eventplanner.data.entities.User

/**
 * Created by chip0llino on 05.02.2018.
 *
 * Represents database helper with all info about tables and columns
 */
@Database(entities = [(User::class), (CalendarEvent::class), (EventPoll::class)], version = DATABASE_VERSION)
@TypeConverters((DbTypeConverters::class))
abstract class EventsDatabase: RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun eventDao(): CalendarEventDao
    abstract fun pollDao(): EventPollDao

    companion object {
        // If you change the database schema, you must increment the database version.
        const val DATABASE_VERSION = 26
        const val DATABASE_NAME = "Events.db"

        private var INSTANCE: EventsDatabase? = null

        fun getInstance(context: Context): EventsDatabase? {
            if (INSTANCE == null) {
                synchronized(EventsDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                            EventsDatabase::class.java, DATABASE_NAME)
                            //TODO: create asynchronous queries
                            .allowMainThreadQueries()
                            .fallbackToDestructiveMigration()
                            .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}