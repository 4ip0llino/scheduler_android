package io.planner.chip0llino.eventplanner.controller.interfaces

import android.content.Context
import io.planner.chip0llino.eventplanner.data.entities.CalendarEvent
import java.util.*

interface RequestController {
    fun initQueue(context: Context)
    fun sendRegisterRequest(username: String, email: String, password: String)
    fun sendLoginRequest(username: String, password: String, controller: LoginController)
    fun sendChangeEventRequest(event: CalendarEvent, token: String, context: Context)
    fun sendDeleteEventRequest(event: CalendarEvent, token: String, context: Context)
    fun sendGetEventDetailsRequest(event: CalendarEvent, token: String, context: Context)
    fun sendInviteRequest(eventname: String, username: String, context: Context)
    fun sendGetEventsForUser(username: String, date: Date, days: Int, context: Context)
    fun sendCreateEventRequest(event: CalendarEvent, username: String, token: String, context: Context)
    fun showLoginError(ctx: Context)
}