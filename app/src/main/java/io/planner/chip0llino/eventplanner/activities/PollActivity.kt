package io.planner.chip0llino.eventplanner.activities

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v7.app.AppCompatActivity
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import io.planner.chip0llino.eventplanner.R
import io.planner.chip0llino.eventplanner.data.entities.EventPoll
import kotlinx.android.synthetic.main.activity_poll.*

class PollActivity : AppCompatActivity() {
    private lateinit var settings: SharedPreferences
    private var pid = 0L
    private lateinit var poll: EventPoll
    private var editable = false

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_poll)

        editable = intent.getBooleanExtra("isEditable", false)

        settings = getSharedPreferences(MainActivity.FILE_NAME, Context.MODE_PRIVATE)
        val userId = 1L//settings.getString(MainActivity.USER_ID, "0").toLong()
        pid = intent.getLongExtra("poll_id", 0)
        poll = if(editable)
        {
            EventPoll(1L, userId, "", hashMapOf(), hashMapOf())
        }
        else
        {
            MainActivity.dataCtrl.getPollById(pid)
        }

        updateUI()

        if(editable)
        {
            btn_vote.text = getString(R.string.event_save)
            btn_vote.setOnClickListener {
                val res = Intent()
                res.putExtra("poll_id", MainActivity.dataCtrl.add(poll))
                setResult(1, res)
                finish()
            }
        }
        else {
            btn_vote.setOnClickListener {
                if (poll.votes.size == 0) {
                    poll.votes = hashMapOf(Pair(userId, list_radio.checkedRadioButtonId.toLong()))
                } else {
                    poll.votes[userId] = list_radio.checkedRadioButtonId.toLong()
                }

                MainActivity.dataCtrl.update(poll)
                updateUI()
            }
        }

        btn_back.setOnClickListener {
            finish()
        }
    }

    private fun createNewAnswerDialog(): AlertDialog.Builder
    {
        val layout = layoutInflater.inflate(R.layout.answer_alert, null)
        val diag = AlertDialog.Builder(this)
        val answerText = layout.findViewById<TextView>(R.id.answerTxt)

        diag.setTitle("Answer text")
                .setView(layout)
                .setPositiveButton(R.string.str_add2) { _, _ ->
                    poll.answers[poll.answers.size + 1L] = answerText.text.toString()
                    updateUI()
                }
                .setNegativeButton(R.string.event_cancel) { dialog, _ ->
                    dialog.cancel()
                }

        return diag
    }

    private fun updateUI()
    {
        poll.name = textView.text.toString()

        list_radio.removeAllViews()

        val density = resources.displayMetrics.density

        textView.setText(poll.name)

        for (ans in poll.answers.toSortedMap(Comparator { t1, t2 -> return@Comparator when {
            t1 < t2 -> -1
            t1 > t2 -> 1
            else -> 0
        } }))
        {
            val rb = RadioButton(this)
            val params = RadioGroup.LayoutParams(this, null)
            val margin = 2*density.toInt()
            val padding_hor = 10*density.toInt()
            val padding_vert = 6*density.toInt()
            val votesNum = poll.votes.values.count { it == ans.key }

            params.setMargins(margin, margin, margin, margin)
            rb.text =  getString(R.string.poll_answer, ans.value, votesNum)
            rb.id = ans.key.toInt()
            rb.textSize = 6.0F*density
            rb.layoutParams = params
            rb.setPadding(padding_hor, padding_vert, padding_hor, padding_vert)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                rb.background = getDrawable(R.drawable.bg_item_poll)
            }

            list_radio.addView(rb)
        }

        if(editable)
        {
            textView.isEnabled = true
            val rb = RadioButton(this)
            val params = RadioGroup.LayoutParams(this, null)
            val margin = 2*density.toInt()
            val padding_hor = 10*density.toInt()
            val padding_vert = 6*density.toInt()

            params.setMargins(margin, margin, margin, margin)
            rb.text =  getString(R.string.str_add)
            rb.id = 0
            rb.textSize = 6.0F*density
            rb.layoutParams = params
            rb.setPadding(padding_hor, padding_vert, padding_hor, padding_vert)
            rb.setOnClickListener { createNewAnswerDialog().show() }

            list_radio.addView(rb)
        }
    }
}
